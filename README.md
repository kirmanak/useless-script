# Introduction

This is a shell (bash, specifically) script, which has the following functions:

1. Print current directory name.
2. Change current directory.
3. Create a file.
4. Let everybody write to a provided file.
5. Remove a file.
